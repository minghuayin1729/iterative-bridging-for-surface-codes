import json
import itertools as it
import pandas as pd
from typing import List, Tuple


class Chain:
    def __init__(self, pins: List[Tuple]):
        self.pins = pins

    def __str__(self):
        return str(self.pins)


# TODO Create Loop class so that data doesn't have to keep being recalculated


class Vertex:
    def __init__(self, i, j):
        self.i = i
        self.j = j

    def __str__(self):
        return f'({self.i}, {self.j})'


class Graph:
    def __init__(self, mat: pd.DataFrame = None):
        # Adjacency matrix
        if mat is None:
            self.mat = pd.DataFrame()
        else:
            self.mat = mat.copy()
        # Currently not used
        self.common_modules = []

    def add_vertex(self, vertex: Vertex):
        self.mat[str(vertex)] = 0
        self.mat.loc[str(vertex)] = 0

    def add_edge(self, edge: Tuple[Vertex]):
        v0, v1 = edge
        try:
            self.mat.loc[str(v0)][str(v1)] = 1
            self.mat.loc[str(v1)][str(v0)] = 1
        except KeyError:
            pass

    def is_edge(self, edge: Tuple[Vertex]):
        v0, v1 = edge
        return bool(self.mat[str(v0)][str(v1)])


def construct_graph(b_chain_sets: List[List[Chain]], l_chain_set: List[Chain]):
    graph = Graph()

    # Vertex construction

    # Find vertices from common modules between b and l
    all_b_pins = []
    for b_chain_set in b_chain_sets:
        for chain in b_chain_set:
            all_b_pins += chain.pins

    all_l_pins = []
    for chain in l_chain_set:
        all_l_pins += chain.pins

    common_pins = list(set(all_b_pins) & set(all_l_pins))
    for p in common_pins:
        i, j = p
        graph.add_vertex(Vertex(i, j))

        # Also keep track of common modules between b and l
        if i not in graph.common_modules:
            graph.common_modules.append(i)

    # Find vertices from common endpoint pins among loops in b
    for l0, l1 in it.combinations(b_chain_sets, 2):
        l0_endpoints = []
        l1_endpoints = []

        for chain in l0:
            l0_endpoints += [chain.pins[0], chain.pins[-1]]
        for chain in l1:
            l1_endpoints += [chain.pins[0], chain.pins[-1]]

        common_pins = list(set(l0_endpoints) & set(l1_endpoints))

        for p in common_pins:
            i, j = p
            graph.add_vertex(Vertex(i, j))

    # Edge construction
    all_chain_sets = b_chain_sets + [l_chain_set]

    # Join endpoints in each loop
    for l in all_chain_sets:
        all_endpoints = []
        for chain in l:
            all_endpoints.append([chain.pins[0], chain.pins[-1]])

        for c0, c1 in it.combinations(all_endpoints, 2):
            for end0, end1 in it.product(c0, c1):
                edge = (Vertex(end0[0], end0[1]), Vertex(end1[0], end1[1]))
                graph.add_edge(edge)

    # Connect vertices corresponding to consecutive pins in a chain
    for l in all_chain_sets:
        for chain in l:
            for p0, p1 in zip(chain.pins, chain.pins[1:]):
                edge = (Vertex(p0[0], p0[1]), Vertex(p1[0], p1[1]))
                graph.add_edge(edge)

    return graph


def find_valid_path(graph: Graph, critical_verts: List[Vertex],
                    b_chain_sets: List[List[Chain]], l_chain_set: List[Chain]):
    # Try every valid connecting order until we find one that preserves the loops
    verts_split = zip(critical_verts[::2], critical_verts[1::2])

    # Can swap endpoint pins within a pair
    with_swaps = []
    for pair in verts_split:
        with_swaps.append((pair, pair[::-1]))

    for pre_path in it.product(*with_swaps):
        path = list(it.chain(*pre_path))
        move_on = False
        for edge in zip(path, path[1:]):
            if not graph.is_edge(edge):
                move_on = True
                break

        if move_on:
            continue
        else:
            # Check if the path preserves the loops
            all_chain_sets = b_chain_sets + [l_chain_set]
            for l in all_chain_sets:
                ordered_modules = []
                for chain in l:
                    ordered_modules += [pin[0] for pin in chain.pins[::2]]

                # Is the path contained in the ordered list of modules?
                ordered_modules_cycle = it.cycle(ordered_modules)
                path_modules = [v.i for v in path[::2]]
                path_length = len(path_modules)
                is_preserved = False

                for _ in ordered_modules:
                    subset = list(it.islice(ordered_modules_cycle,
                                            0, path_length))
                    if path_modules == subset or path_modules[::-1] == subset:
                        is_preserved = True
                        break

                if is_preserved:
                    return list(path)

    return None


def create_max_priority_queue(base_loops: List[List[Chain]],
                              candidate_loops: List[List[Chain]]):
    max_priority_queue = []

    these_modules = set([pin[0] for loop in base_loops for chain in loop
                         for pin in chain.pins])

    for index, l in enumerate(candidate_loops):
        those_modules = set([pin[0] for chain in l
                             for pin in chain.pins])
        common_modules = these_modules & those_modules
        num_common_modules = len(common_modules)
        if num_common_modules > 0:
            loop_data = {'index': index,
                         'num_common_modules': num_common_modules,
                         'common_modules': common_modules}
            max_priority_queue.append(loop_data)

    max_priority_queue.sort(key=lambda data: data['num_common_modules'],
                            reverse=True	)
    return max_priority_queue


# This code is awful lmaoooo
def update_chain_sets(b_chain_sets: List[List[Chain]], path: List[Vertex]):
    for edge in zip(path, path[1:]):
        for index, l in enumerate(b_chain_sets):
            l_copy = l.copy()

            # Merge if valid
            first_chain = None
            for chain in l_copy:
                # Bruhhh I hate how I've written this 💀
                if (edge[0].i, edge[0].j) in [chain.pins[0], chain.pins[-1]] or \
                        (edge[1].i, edge[1].j) in [chain.pins[0], chain.pins[-1]]:
                    first_chain = chain

                    try:
                        first_index = \
                            [chain.pins[0],
                             chain.pins[-1]].index((edge[0].i, edge[0].j))
                    except ValueError:
                        first_index = \
                            [chain.pins[0],
                             chain.pins[-1]].index((edge[1].i, edge[1].j))

                    break

            if first_chain is not None:
                l_copy.remove(first_chain)
                second_chain = None
                for chain in l_copy:
                    if (edge[0].i, edge[0].j) in [chain.pins[0], chain.pins[-1]] or \
                            (edge[1].i, edge[1].j) in [chain.pins[0], chain.pins[-1]]:
                        second_chain = chain

                        try:
                            second_index = \
                                [chain.pins[0],
                                 chain.pins[-1]].index((edge[0].i, edge[0].j))
                        except ValueError:
                            second_index = \
                                [chain.pins[0],
                                 chain.pins[-1]].index((edge[1].i, edge[1].j))

                        break

                if second_chain is not None:
                    l_copy.remove(second_chain)
                    # Merge!
                    new_pins = first_chain.pins[::(1 if first_index == 1 else -1)] + \
                        second_chain.pins[::(1 if second_index == 0 else -1)]
                    new_chain = Chain(new_pins)
                    l_copy.append(new_chain)

                    b_chain_sets[index] = l_copy


def iterative_bridging(filename):
    # Read JSON data file
    modular_data = json.load(open(filename))

    # Create starting chain set for each loop
    dual_loops = []
    for loop in modular_data['loop_penetration']:
        chain_set = []
        for m in loop:
            chain_set.append(Chain([(m, 0), (m, 1)]))
        dual_loops.append(chain_set)

    all_bridge_structures = []
    # Main loop - while dual_loops is not empty
    while dual_loops:
        # Initial bridge structure b
        bridge_structure = [dual_loops.pop(0)]

        # Get max priority queue
        max_priority_queue = create_max_priority_queue(bridge_structure,
                                                       dual_loops)

        # While max_priority_queue is not empty
        while max_priority_queue:
            loop_data = max_priority_queue[0]
            l_chain_set = dual_loops[loop_data['index']]
            # Construct bridge graph
            graph = construct_graph(bridge_structure, l_chain_set)

            # Determine the critical vertices
            critical_verts = []
            for m in loop_data['common_modules']:
                critical_verts += [Vertex(m, 0), Vertex(m, 1)]

            path = find_valid_path(graph, critical_verts,
                                   bridge_structure, l_chain_set)
            if path is not None:
                bridge_structure.append(l_chain_set)
                update_chain_sets(bridge_structure, path)

                dual_loops.remove(l_chain_set)
                # Get new max priority queue
                max_priority_queue = create_max_priority_queue(bridge_structure,
                                                               dual_loops)

        all_bridge_structures += bridge_structure

    return all_bridge_structures


if __name__ == '__main__':
    all_bridge_structures = iterative_bridging('primal-modules.json')

    print('Here are the chain sets for each loop:\n------------------')
    for index, chain_set in enumerate(all_bridge_structures):
        to_print = '{{ '
        to_print += ', '.join([str(chain_repr)
                              for chain_repr in [chain.pins for chain in chain_set]])
        to_print += ' }}'
        print(to_print)
